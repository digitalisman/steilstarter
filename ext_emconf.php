<?php

/**
 * Extension Manager/Repository config file for ext "steilstarter".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Steilstarter',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'bootstrap_package' => '13.0.0-14.9.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Steilstarter\\Steilstarter\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Vogel',
    'author_email' => 'info@digitalisman.de',
    'author_company' => 'Steilstarter',
    'version' => '1.0.0',
];
